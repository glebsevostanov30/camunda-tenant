package ru.example.camunda.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class CamundaTenantController {
    private final RuntimeService runtimeService;

    @GetMapping("/start/process/green")
    public void startTenantActive(){
        runtimeService
                .createProcessInstanceByKey("my-project-process")
                .processDefinitionTenantId("green")
                .execute();
    }

    @GetMapping("/start/process/blue")
    public void startTenantPassive(){
        runtimeService
                .createProcessInstanceByKey("my-project-process")
                .processDefinitionTenantId("blue")
                .execute();
    }

    @GetMapping("/correlate/process/business-key/{key}/green")
    public void correlateTenantActive(@PathVariable String key){
        runtimeService
                .createMessageCorrelation("MessageCorrelation")
                .processInstanceBusinessKey(key)
                .correlate();
    }

    @GetMapping("/correlate/process/business-key/{key}/blue")
    public void correlateTenantPassive(@PathVariable String key){
        runtimeService
                .createMessageCorrelation("MessageCorrelation")
                .processInstanceBusinessKey(key)
                .correlate();
    }
}
