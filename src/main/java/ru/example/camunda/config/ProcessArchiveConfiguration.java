package ru.example.camunda.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.DeploymentBuilder;
import org.camunda.bpm.engine.repository.DeploymentWithDefinitions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.camunda.bpm.spring.boot.starter.property.CamundaBpmProperties.*;
import static org.springframework.core.io.support.ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class ProcessArchiveConfiguration {
    private final static String PROCESS_APPLICATION = "Process application";


    @Bean
    public RepositoryService deployProcessArchive(CamundaTenantProperties camundaTenantProperties,
                                                  RepositoryService repositoryService) throws IOException {
        addDeploy(camundaTenantProperties, repositoryService);
        return repositoryService;

    }

    private void addDeploy(CamundaTenantProperties camundaTenantProperties,
                           RepositoryService repositoryService) {
        camundaTenantProperties.archives.stream()
                .map(processArchive -> this.extractDeploymentInformation(processArchive, repositoryService))
                .forEach(deploymentBuilder -> {
                    DeploymentWithDefinitions deployment = deploymentBuilder.deployWithResult();
                    log.info("{}", deployment);
                });
    }

    private DeploymentBuilder extractDeploymentInformation(CamundaTenantProperties.ProcessArchive processArchive,
                                                           RepositoryService repositoryService) {
        var deploymentBuilder = repositoryService.createDeployment()
                .name(processArchive.getName())
                .source(PROCESS_APPLICATION)
                .tenantId(processArchive.getTenant())
                .enableDuplicateFiltering(true);

        var initDeploymentResourcePattern = initDeploymentResourcePattern(processArchive.getPath());
        initDeploymentResourcePattern
                .forEach(resourceClassPathSuffix -> {
                    try {
                        Arrays.stream(new PathMatchingResourcePatternResolver().getResources(resourceClassPathSuffix))
                                .filter(Resource::isFile)
                                .forEach(resourceBpmn -> {
                                    try {
                                        File bpmnFile = resourceBpmn.getFile();
                                        deploymentBuilder.addInputStream(bpmnFile.getName(), new FileInputStream(bpmnFile) );
                                    } catch (IOException e) {
                                        throw new RuntimeException(e);
                                    }
                                });
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
        return deploymentBuilder;
    }

    private List<String> initDeploymentResourcePattern(String filePath) {
        final Set<String> suffixes = new HashSet<>();
        suffixes.addAll(Arrays.asList(DEFAULT_DMN_RESOURCE_SUFFIXES));
        suffixes.addAll(Arrays.asList(DEFAULT_BPMN_RESOURCE_SUFFIXES));
        suffixes.addAll(Arrays.asList(DEFAULT_CMMN_RESOURCE_SUFFIXES));

        final Set<String> patterns = new HashSet<>();
        for (String suffix : suffixes) {
            patterns.add(String.format("%s%s/**/*.%s", CLASSPATH_ALL_URL_PREFIX, filePath, suffix));
        }

        return patterns.stream().map(String::new).toList();
    }
}
