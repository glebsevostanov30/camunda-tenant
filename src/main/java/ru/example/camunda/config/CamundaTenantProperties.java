package ru.example.camunda.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("camunda.bpm.deployment")
public class CamundaTenantProperties {
    List<ProcessArchive> archives;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ProcessArchive{
        private String name;
        private String tenant;
        private String path;
    }
}
